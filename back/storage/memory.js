const mx = module.exports = {};

function Memory(){

  const pool = [];
  const pmap = {};

  this.expose = function(option){
    const actions = {
      alloc: function(opt){
        if (!pmap[opt.key]){
          pmap[opt.key] = []
        }
        const globalIndex = pool.push(opt.value) - 1;
        const localIndex = pmap[opt.key].push(globalIndex) -1;
        return [globalIndex, opt.key, localIndex]
      },
      getByKey: function(opt){
        let ver = opt.version || 0;
        if (ver < 0){
          ver = pmap[opt.key].length -1
        }
        return pool[pmap[opt.key][ver]]
      },
      getByIndex: function(index){
        return pool[index]
      }
    };

    return actions[option.action](option.token)
  }

}

Memory.prototype.allocs = function(){
  return Object.freeze([
    'function',
    'object',
    'number'
  ])
};

Memory.prototype.alloc = function(key, value){
  if(typeof key === 'string' && this.allocs().indexOf(typeof value) > -1)
  return this.expose({action: "alloc", token: {key:key, value:value}})
}

Memory.prototype.find = function(){
  if (typeof arguments[0][0] === "number"){
    return this.expose({action: "getByIndex", token: arguments[0][0]})
  }
  return this.expose({action: "getByKey", token: {key:arguments[0][0], version: arguments[0][1]}})
}

Memory.prototype.last = function(){
  return this.expose({action: "getByKey", token: {key:arguments[0][0], version: -1}})
}

mx.Memory = Memory

mx.malloc = function(){

  var mem = {
    init: function () {
      Memory.call(this);
    },
    alloc: function(opt){
      return Memory.prototype.alloc.call(this, opt)
    },
    find: function(){
      return Memory.prototype.find.call(this, arguments)
    },
    last: function(){
      return Memory.prototype.last.call(this, arguments)
    },
    falloc: function(key, value){
      return Memory.prototype.alloc.call(this, {key:key, value:value})
    },
    allocs: function(){
      return Memory.prototype.allocs.call(this)
    }
  };
  Object.create(mem);
  mem.init();
  return mem
};

mx.test = function(m, times){
  const loop = times || 100;
  function runIndexTest(){
    console.time("test write");
    for(let i=0;i<loop;i++){
      m.falloc("Number "+i, i*loop)
    };
    console.timeEnd('test write');

    console.time("test read");
    for(let i=0;i<loop;i++){
      m.find(i*loop)
    };
    console.timeEnd('test read');
    return 0
  }
  return runIndexTest();
}
