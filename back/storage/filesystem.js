const mx = module.exports = {};

function Filesystem(option){
  const root = option.root || __dirname;
  const paths = {};
}

Filesystem.prototype.addPath = function(option){
  paths[option.key] = option.value;
}

mx.mount = function(option){

  // object create
  var fsm = {
    init: function (opt) {
      Filesystem.call(this, opt);
      return this;
    },
    addPath: function(opt){
      Filesystem.prototype.call(this, opt);
      return this
    }
  };
  Object.create(fsm);
  fsm.init(option);

  return fsm
}
