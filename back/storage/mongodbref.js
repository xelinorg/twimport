const mx = module.exports = {};

const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

const url = 'mongodb://localhost:27017';

function DBRef(option){

  const _dbref = [];
  MongoClient.connect(url, { useNewUrlParser: true }, function(err, client) {
    assert.equal(null, err);
    _dbref.push(client)
  });

  this.expose = function(option){
    return _dbref[0].db(option.db).command(option.command)
  };

  this.pipeline = function(option){
    return _dbref[0].db(option.db).collection(option.collection).aggregate(option.stages)
  }

};

DBRef.prototype.find = function findDBRef(option){
  return this.expose({db: option.db, command: {find: option.collection, filter: option.filter}})
};

DBRef.prototype.insert = function insertDBRef(option){
  return this.expose({db: option.db, command: {insert: option.collection, documents: option.documents}})
};

DBRef.prototype.aggregate = function aggregateDBRef(option){
  return this.expose({db: option.db, command: {aggregate: option.collection, pipeline: option.pipeline, cursor: option.cursor ||  {}}})
};

DBRef.prototype.next = function nextDBRef(option){
  // {
  //   action: { endpoint: '/import', method: 'POST' },
  //   payload: { twid: 'xelinorg' }
  // }

  const collection = option.action.endpoint.replace('/','');
  const method = option.action.method;
  const payload = option.payload;
  console.log('DBRef.prototype.next', collection, method, payload);

  // if (method === 'GET'){
  //   return this.find({db: 'twimport', collection: collection, filter: payload})
  // }
  //
  // if (method === 'POST'){
  //   return this.insert({db: 'twimport', collection: collection, documents: [payload]})
  // }

  const _dbref = this;
  return new Promise(function(resolve, reject){

    if (method === 'GET'){
      return _dbref.find({db: 'twimport', collection: collection, filter: payload}).then(
        function dbGetSuccess(result){
          return resolve(result)
        },
        function dbGetFailure(err){
          return reject(err)
        }
      )
    }

    if (method === 'POST'){
      return _dbref.insert({db: 'twimport', collection: collection, documents: [payload]}).then(
        function dbPostSuccess(result){
          return resolve(result)
        },
        function dbPostFailure(err){
          return reject(err)
        }
      )
    }


  })
};

function createDB(option){
  return new DBRef(option || {})
};


mx.createDB = createDB
