const mx = module.exports = {};

mx.twitterAuthOptions = function(option){

  if (!option || !option.BTCBase64 || typeof option.BTCBase64 !== 'string'){
    return
  }

  const payload = 'grant_type=client_credentials'

  const localoption = {
    payload: payload,
    headers: {
      'Authorization': 'Basic ' + option.BTCBase64,
      'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
      'Content-Length': Buffer.byteLength(payload),
      'Accept-Encoding': 'gzip'
    },
    hostname: 'api.twitter.com',
    port: 443,
    path: '/oauth2/token',
    method: 'POST'
  }

  option.net.utils.fixResolve(option, localoption)

  return localoption;

}

mx.twitterAuth = function(option){
  const opt = mx.twitterAuthOptions(option);
  option.test(opt)
}
