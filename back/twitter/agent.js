const mx = module.exports = {};

mx.twitterShowUser = function(option){

  const showID = function(result){
    console.log('Id ' + result.data.id + ' for screen name ' + result.data.screen_name)
  }

  if(typeof option === 'object'){
    if (option.headers.Authorization.indexOf('Bearer') === 0){
      option.prs = showID;
      // TODO replace with resource exec next line
      mx.test(option)
    }else if(option.headers.Authorization.indexOf('Basic') === 0){
      const pbearer = function(result){
        const showopts = mx.twitterShowUserOption({bearer: result.data.access_token, screen_name: option.screen_name});
        showopts.prs = showID;
        // TODO replace with resource exec next line
        mx.test(showopts)
      }
      const authopt = option.twitterAuthOption(option);
      authopt.prs = pbearer;
      // TODO replace with resource exec next line
      mx.testTwitterAuth(authopt)
    }
    else{
      console.log('can not test testTwitterUserShow without BTCBase64 or access_token')
    }
  }

}

mx.twitterShowUserOption = function(option){

  if (!option || typeof option.access_token !== 'string' || typeof option.screen_name !== 'string'){
    return
  }

  const localoption = {
    headers: {
      'Authorization': 'Bearer ' + option.access_token,
    },
    hostname: 'api.twitter.com',
    port: 443,
    path: '/1.1/users/show.json?screen_name=' + option.screen_name,
    method: 'GET'
  }

  return localoption;

}

mx.twitterFollowersIdsOption = function(option){

  if (typeof option !== 'object' || typeof option.access_token !== 'string' || typeof option.screen_name !== 'string'){
    return
  }
  const localoption = {
    headers: {
      'Authorization': 'Bearer ' + option.access_token,
    },
    hostname: 'api.twitter.com',
    port: 443,
    path: '/1.1/followers/ids.json?' + fixPath(option),
    method: 'GET',
    trap: [
      'x-rate-limit-reset',
      'x-rate-limit-remaining',
      'trap-req-path'
    ]
  };

  return localoption

}

mx.testTwitterFollowersIds = function(option){

  const countFollowers = function(result){
    let userslength = result.data && result.data.ids ? result.data.ids.length : 0;
    console.log('There are ' + userslength + ' follower ids on this query with next cursor ' + result.data.next_cursor_str);
    console.log('Metadata rate limit and and reset are ', Object.entries(result.metadata).join(' '));
    if(parseInt(result.data.next_cursor_str) > 0 && result.metadata['x-rate-limit-remaining'] > 0 ){
      option.path = fixCursor(option.path, result.data.next_cursor_str);
      option.prs = countFollowers
      mx.testTwitterFollowersIds(option)
    }

  }

  if(typeof option === 'object'){

    if (option.headers.Authorization.indexOf('Bearer') === 0){
      option.prs = countFollowers;
      mx.test(option)

    }else if(option.headers.Authorization.indexOf('Basic') === 0){
      const pbearer = function(result){
        const follopts = mx.twitterFollowersIdsOption({access_token: result.data.access_token, screen_name: option.screen_name});
        follopts.prs = countFollowers
        mx.test(showopts)
      }
      const authopt = mx.twitterAuthOption(option);
      authopt.prs = pbearer;
      mx.testTwitterAuth(authopt)
    }
    else{
      console.log('can not test testTwitterFollowersIds without BTCBase64 or access_token')
    }
  }

}

function TwitterAgent(option) {
  this.resource = option.resource;

}

function bootAgent(){
  
}
