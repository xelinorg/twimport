// GET https://api.twitter.com/1.1/followers/list.json?count=100&screen_name=realDonaldTrump
// https://api.twitter.com/1.1/followers/list.json?count=100&screen_name=realDonaldTrump&skip_status=true&include_user_entities=false&cursor=1613873681058278841

const m = module.exports = {};

function boot(option){
  const base = getSampleOption();
  const extension = {
    'bearer': option.bearer,
    'screen_name': option.screen_name,
    'cursor': option.cursor || 0,
    'count': option.count || 0
  }
  const localoption = extendBase(base, extension);
  getList(localoption)
}

function extendBase(base, extension){
  const ext = Object.assign(base, extension);
  return ext
}

function fixPath(option){
  let extended = "";

  extended += "&screen_name=" + option.screen_name;
  extended += "&skip_status=true";
  extended += "&include_user_entities=false";
  option.cursor && (extended += "&cursor=" + option.cursor);
  option.count && (extended += "&count=" + option.count);

  return option.path + extended

}

function getSampleOption(){
  return {
    hostname: "api.twitter.com",
    port: 443,
    path: "/1.1/followers/list.json?"
  }
}

function getList(option){

  const options = {
    hostname: option.hostname,
    port: option.port,
    path: fixPath(option),
    method: "GET",
    headers: {
      'User-Agent': "echolocate v0.1.1",
      'Authorization': "Bearer " + option.bearer,
      'Accept-Encoding': "gzip"
    }
  };

  const req = https.request(options, function(res){

    const resettime = parseInt(res.headers['x-rate-limit-reset']);
    const timewindow = new Date(1000*resettime);
    const remaining = parseInt(res.headers['x-rate-limit-remaining']);

    var gunzip = zlib.createGunzip();
    res.pipe(gunzip);

    const datap = [];

    gunzip.on('data', function(data) {
      datap.push(data.toString())
    }).on("end", function() {
      const jsonp = JSON.parse(datap.join(""));

      console.log(jsonp.users.length, jsonp.next_cursor, jsonp.next_cursor_str, timewindow, remaining)
      // if x-rate-limit-remaining > 0
      //   setTimeout with zero
      // else
      //   setTimeout with x-rate-limit-reset
      //

      // use x-connection-hash to track the connection
      // extract next cursor
    }).on("error", function(e) {
      console.log("gzip problem", e.message)
    })
  });

  req.on('error', function(e) {
    console.error("problem with request: ", e.message);
  });

  req.end()

}

m.boot = boot
