//sse to express to get work and to propagate job results
// will use the twitter module to access the twitter api
const mx = module.exports = {};


/*
twitter handle statuses
WAITTING
IMPORTINGIDS
IMPORTINGFOLLOWERS
UPDATED
*/

function Worker(option){
  this.eventsource = option.eventsource
}


mx.createWorker = function(option){
  const opt = option || {};
  const w = new Worker(opt);
  return w
}
