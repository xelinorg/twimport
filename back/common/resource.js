const mx = module.exports = {};

function Resource(option){
  const config = {
    //path: '/'
  };
  this.configf = option.configf(config)
  this.reqp = option.reqp;
  this.pathUpdate = function(option){
    if(typeof option === 'object' && typeof option.path === 'string' && option.path.indexOf('/') === 0){
      config.path = option.path
    }
  }

};

Resource.prototype.access = function(option){
  const conf = this.configf();
  return this.reqp(conf).then(this.configf(option))
};

Resource.prototype.loadConfig = function(config){
  const confstack = Object.entries(config);
  for (let c=0;c<confstack.length;c++){
    this.configf({key: confstack[c][0], value: confstack[c][1]})
  }
  return this;
}

mx.createResource = function(option){
  if (typeof option !== 'object' || typeof option.configf !== 'function' || typeof option.reqp !== 'function'){
    return
  }
  const opt = {};
  opt.configf = option.configf;
  opt.reqp = option.reqp;
  const r = new Resource(opt);
  return r
}
