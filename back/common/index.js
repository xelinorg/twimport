const util = require('util');
const https = require('https');
const mx = module.exports = {};

const successf = function successf(){console.log('success ', arguments)};
const errorf = function errorf(){console.log('error ', arguments)};

function esparse(chunk){
  const bulk = chunk.split('\n\n');
  const cleanLines = bulk.map(function(item){
    return item.replace('\n')
  });

  return cleanLines.reduce(function(bucket, line){

    const eshead = line.split(':', 1);
    const esbody = line.slice(line.indexOf(':') +1).replace('\n','')

    if (eshead.length >0 && eshead[0].replace(' ','') === 'data' && typeof esbody === 'string'){
      bucket.push(esbody)
    };
    return bucket

  }, []);

}

function bearerTokenCredentials(key, secret){
  return encodeURIComponent(key) + ':' + encodeURIComponent(secret)
}

function base64EncodedBearerTokenCredentials(option){
  const BTC = bearerTokenCredentials(option.key, option.secret);
  return Buffer.from(BTC).toString('base64');
}

function fixCursor(path, cursor){

  if (path.indexOf('&cursor=') < 0){
    return path + '&cursor=' + cursor;
  }

  return path.replace( /cursor=[\d]+/g, 'cursor=' + cursor);

}

function fixResolve(optionfrom, optionto){
  optionto.prs = optionfrom.prs || successf;
  optionto.pre = optionfrom.pre || errorf
  return optionto
}

function fixPayload(option, payload, headers){

  const base = {
    data: payload,
  };

  if(option.trap){

    base.metadata = option.trap.reduce(function(bucket, item){
      if (headers[item]){
        bucket[item] = headers[item]
      }
      return bucket
    }, {});

    if (option.trap.indexOf('trap-req-path')>-1){
      base.metadata.path = option.path;
    }
  }

  return base

}

function fixPath(option){
  let fixed = '';

  fixed += '&screen_name=' + option.screen_name;
  fixed += '&skip_status=true';
  fixed += '&include_user_entities=false';
  option.cursor && (fixed += '&cursor=' + option.cursor);
  option.count && (fixed += '&count=' + option.count);

  return fixed

}

function netreq(option, callback){

  // option for the request

  const payload = option.payload || 0;

  const localoption = {
    hostname: option.hostname,
    port: option.port,
    path: option.path,
    method: option.method,
    headers: {
      'User-Agent': 'echolocate v0.1.1',
    }
  };

  if (option.headers){
    Object.entries(option.headers).reduce(function(bucket, header){
      bucket[header[0]] = header[1];
      return bucket
    }, localoption.headers);
  }

  const req = https.request(localoption, function(res){

    const src = res.headers['content-encoding'] === 'gzip' ? res.pipe(zlib.createGunzip()) : res;

    readerp(src).then(
      function(result){
        callback(null, fixPayload(option, result, res.headers))
      },
      function(err){
        callback({'request res error': err})
      }
    )

  });

  req.on('error', function(err) {
    //reject the request
    callback({'request error': err})
  });

  // if have payload write is
  if(payload){
    req.write(payload);
  }

  //end the request
  req.end()

}

function reqp(option){
  if(typeof option !== 'object'){
    return
  }
  const p = util.promisify(netreq);
  return p(option)
}

function readerp(src){
  const p = util.promisify(reader);
  return p(src)
}

function reader(req, cb){
  const datap = [];

  req.on('data', function(data) {
    datap.push(data.toString())
  }).on('end', function() {
    //resolve the request
    cb(null, JSON.parse(datap.join('')))
  }).on('error', function(e) {
    //reject the request res error
    cb({'reader error': err})
  })
}

function configf(container){

  const validator = {
    set: function(obj, prop, value) {
      return true;
    }
  };

  return function innerconfigf(option){
    if (typeof option === 'string') {
      return container[option];
    }

    if (option && !container[option.key] && typeof option.key === 'string' && option.value ) {
      container[option.key] = option.value
    }

    if (!option) {
      return new Proxy(container, validator);
    }
  }
}

mx.configf = configf;

mx.base64EncodedBearerTokenCredentials = base64EncodedBearerTokenCredentials;

mx.fbauthStatus = function facebookAuthenticationStatus(option){
  if (typeof option === 'object' && typeof option.token === 'string'){
    const opt = mx.testOpt();
    opt.hostname = 'graph.facebook.com';
    opt.path = '/v2.8/me?method=get&suppress_http_code=1&access_token=' + option.token;
    typeof option.prs === 'function' && ( opt.prs = option.prs);
    typeof option.pre === 'function' && ( opt.pre = option.pre);

    mx.test(opt)
  }
};

mx.net = {
  req: reqp,
  reader: readerp,
  utils: {
    fixResolve: fixResolve
  },
  middleware:{
    cors: function netMiddlewareCORS(req, res, next) {
      if (req.headers.origin){
        //console.log('cors is on :', req.headers.origin );
        res.set({
          'Access-Control-Allow-Origin': req.headers.origin,
          'Access-Control-Allow-Credentials': true,
          'Access-Control-Expose-Headers': '*'
        })
        if(req.method === 'OPTIONS'){
          const requestedHeaders = req.headers['access-control-request-headers'];
          if(requestedHeaders){
            res.set({
              'Access-Control-Allow-Headers': requestedHeaders
            })
          }
        }

      }
      next()
    },
    sse: function netMiddlewareSSE(option){
      //console.log('common net middleware option is', option.next);
      const configf = option.engine.configf.bind(option.engine);
      const register = option.engine.register.bind(option.engine);

      return function netMiddlewareeventEngine(req, res, next) {
        if (!configf('topology')){
          return res.status(400).send('no events on this server')
        }
        register({req:req, res:res});
        next()
      }
    }
  }
};

mx.createResource = require('./resource').createResource;

mx.createEventEngine = require('./eventengine').createEventEngine;

//
// these test should be moved out of this file. parts could be used for the twitter module
//
mx.test = function(option){
  if (typeof option !== 'object'){
    console.log('provide an option object like ', JSON.stringify(mx.testOpt()))
    return
  }
  //console.log('test option is ', option)
  const prs = option.prs || successf;
  const pre = option.pre || errorf;

  mx.net.req(option).then(prs, pre)
}

mx.testOpt = function(){
  const opts = {};
  opts.prs = null;
  opts.pre = null;
  opts.hostname = 'systemics.gr';
  opts.port = 443;
  opts.path = '/';
  opts.method = 'GET';

  return opts
}

mx.testTwitterAuth = function(option){
  const opt = mx.twitterAuthOptions(option);
  mx.test(opt)
}

mx.testTwitterUserShow = function(option){

  const showID = function(result){
    console.log('Id ' + result.data.id + ' for screen name ' + result.data.screen_name)
  }

  if(typeof option === 'object'){
    if (option.headers.Authorization.indexOf('Bearer') === 0){
      option.prs = showID;
      mx.test(option)
    }else if(option.headers.Authorization.indexOf('Basic') === 0){
      const pbearer = function(result){
        const showopts = mx.twitterShowUserOptions({bearer: result.data.access_token, screen_name: option.screen_name});
        showopts.prs = showID;
        mx.test(showopts)
      }
      const authopt = mx.twitterAuthOptions(option);
      authopt.prs = pbearer;
      mx.testTwitterAuth(authopt)
    }
    else{
      console.log('can not test testTwitterUserShow without BTCBase64 or access_token')
    }
  }

}

mx.testTwitterFollowersIds = function(option){

  const countFollowers = function(result){
    let userslength = result.data && result.data.ids ? result.data.ids.length : 0;
    console.log('There are ' + userslength + ' follower ids on this query with next cursor ' + result.data.next_cursor_str);
    console.log('Metadata rate limit and and reset are ', Object.entries(result.metadata).join(' '));
    if(parseInt(result.data.next_cursor_str) > 0 && result.metadata['x-rate-limit-remaining'] > 0 ){
      option.path = fixCursor(option.path, result.data.next_cursor_str);
      option.prs = countFollowers
      mx.testTwitterFollowersIds(option)
    }

  }

  if(typeof option === 'object'){

    if (option.headers.Authorization.indexOf('Bearer') === 0){
      option.prs = countFollowers;
      mx.test(option)

    }else if(option.headers.Authorization.indexOf('Basic') === 0){
      const pbearer = function(result){
        const follopts = mx.twitterFollowersIdsOptions({access_token: result.data.access_token, screen_name: option.screen_name});
        follopts.prs = countFollowers
        mx.test(showopts)
      }
      const authopt = mx.twitterAuthOptions(option);
      authopt.prs = pbearer;
      mx.testTwitterAuth(authopt)
    }
    else{
      console.log('can not test testTwitterFollowersIds without BTCBase64 or access_token')
    }
  }

}

mx.twitterAuthOptions = function(option){

  if (!option || !option.BTCBase64 || typeof option.BTCBase64 !== 'string'){
    return
  }

  const payload = 'grant_type=client_credentials'

  const localoption = {
    payload: payload,
    headers: {
      'Authorization': 'Basic ' + option.BTCBase64,
      'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
      'Content-Length': Buffer.byteLength(payload),
      'Accept-Encoding': 'gzip'
    },
    hostname: 'api.twitter.com',
    port: 443,
    path: '/oauth2/token',
    method: 'POST'
  }

  fixResolve(option, localoption)

  return localoption;

}

mx.twitterShowUserOptions = function(option){

  if (!option || typeof option.access_token !== 'string' || typeof option.screen_name !== 'string'){
    return
  }

  const localoption = {
    headers: {
      'Authorization': 'Bearer ' + option.access_token,
    },
    hostname: 'api.twitter.com',
    port: 443,
    path: '/1.1/users/show.json?screen_name=' + option.screen_name,
    method: 'GET'
  }

  return localoption;

}

mx.twitterFollowersIdsOptions = function(option){

  if (typeof option !== 'object' || typeof option.access_token !== 'string' || typeof option.screen_name !== 'string'){
    return
  }
  const localoption = {
    headers: {
      'Authorization': 'Bearer ' + option.access_token,
    },
    hostname: 'api.twitter.com',
    port: 443,
    path: '/1.1/followers/ids.json?' + fixPath(option),
    method: 'GET',
    trap: [
      'x-rate-limit-reset',
      'x-rate-limit-remaining',
      'trap-req-path'
    ]
  };

  return localoption

}

mx.testResource = function(){
  const r = mx.createResource({reqp: reqp, configf: configf});
  const c = mx.testOpt();
  c.path= '/tmp/test.json';
  c.prs = function(res){console.log('running testResource', res)};
  r.loadConfig(c);
  r.access('prs')
};

mx.testTwitterResource = function(option){

  const twitterAuthRes = mx.createResource();
  const followerIdsRes = mx.createResource();

  const countFollowers = function(result){
    let userslength = result.data && result.data.ids ? result.data.ids.length : 0;
    console.log('There are ' + userslength + ' follower ids on this query with next cursor ' + result.data.next_cursor_str);
    console.log('Metadata rate limit and and reset are ', Object.entries(result.metadata).join(' '));
    if(parseInt(result.data.next_cursor_str) > 0 && result.metadata['x-rate-limit-remaining'] > 0 ){
      const pathUpdate = fixCursor(followerIdsRes.configf('path'), result.data.next_cursor_str);
      followerIdsRes.pathUpdate({path: pathUpdate});
      followerIdsRes.access('countFollowers')
    }

  }

  const tokenResolver =  function(result){
    //console.log('tokenResolver result', result)
    const follopt =  mx.twitterFollowersIdsOptions({screen_name:option.screen_name, access_token: result.data.access_token});
    followerIdsRes.loadConfig(follopt);
    followerIdsRes.configf({key: 'countFollowers', value: countFollowers});
    followerIdsRes.access('countFollowers')
  };

  const twaopt = mx.twitterAuthOptions({BTCBase64: option.BTCBase64});
  twitterAuthRes.loadConfig(twaopt);
  twitterAuthRes.configf({key: 'resolveToken', value: tokenResolver});
  twitterAuthRes.access('resolveToken')


}

mx.testEventEngine = function(option){
  const opt = option || {};
  let ssebooted = false;
  let failcount = 0;

  const certificate = fs.readFileSync('/home/ale3/dev/twimport/back/crypto/server.cert');

  const esoption = {
    hostname: opt.hostname || 'localhost',
    port: opt.port || 3000,
    path: opt.path || '/loop?',
    protocol: 'https:',
    rejectUnauthorized: true,
    agent: false,
    ca: [certificate],
    ssekey: 'key=' + (opt.key ? opt.key : 'worker_' + Math.floor(Math.random() * Math.floor(Number.MAX_SAFE_INTEGER))),
    secret: '&secret=' + (opt.key ? opt.key : '' + Math.floor(Math.random() * Math.floor(Number.MAX_SAFE_INTEGER))),
    watch: opt.watch ? '&watch='+opt.watch : '',
    extra :[],
    method: 'GET',
    headers: {
      'User-Agent': 'echolocate v0.1.1',
      'Accept' : 'text/event-stream',
      'Connection': 'keep-alive'
    }
  };

  const messages = [];

  const esreq = function(option){
    option.basepath = ssebooted ? option.path : option.path  + option.ssekey + option.secret + option.watch
    option.path = option.basepath + option.extra.join('&')
    const req = https.request(option, function(res){
      // we need a reader like this to read on data and not on end
      ssebooted = true;
      const datap = [];
      let dpindex = 0;

      res.on('data', function(data) {
        failcount = 0;
        const next = datap.join('') + data.toString();
        const message = esparse(next);
        if (message){
          console.log('message received');
          console.log(message);
          datap.splice(0, datap.length);
          messages.push(message)
        }

        datap.push(data)
      }).on('end', function() {
        //resolve the request
        console.log('common sse test onend', datap.join(''))
        if (failcount < 3){
          setTimeout(function(){
            failcount += failcount;
            esreq(option)
          },3000)
        }

      }).on('error', function(e) {
        //reject the request res error
        console.log('common sse test onend', e)
      })

    });

    req.on('error', function(err) {
      //reject the request
      console.log(err)
    });

    //end the request
    req.end()

    return req
  }

  return {
    esmessages: function(){
      return messages;
    },
    esoption: esoption,
    esreq: esreq
  }

}

mx.testAll = function(option){
  const screen_name = option.screen_name || 'systemicsgr';
  const topt0 = mx.testOpt();
  mx.test(topt0);

  topt0.hostname = 'systemics.gr';
  topt0.path = '/tmp/test.json';
  topt0.port = 443;
  mx.test(topt0)

  if (typeof option === 'object' && option.BTCBase64){
    const topt1 = mx.twitterAuthOptions({BTCBase64: option.BTCBase64});
    mx.testTwitterAuth(topt1)
  } else {
    console.log('no BTCBase64 option skipping testTwitterAuth')
  }

  if (typeof option === 'object' && option.access_token){
    const topt2 = mx.twitterShowUserOptions({access_token: option.access_token, screen_name: screen_name});
    mx.testTwitterUserShow(topt2)
    const topt3 = mx.twitterFollowersIdsOptions({access_token: option.access_token, screen_name: screen_name});
    mx.testTwitterFollowersIds(topt3)
  } else {
    console.log('no access_token option skipping testTwitterUserShow and testTwitterFollowersIds without testTwitterAuth')
  }

  if (typeof option === 'object' && option.BTCBase64 && option.doubleCheck){
    const topt4 = mx.twitterAuthOptions({BTCBase64: option.BTCBase64})
    if (option.doubleCheck){
      topt4.prs = function callbackToGetAccessToken(result){
        const topt5 = mx.twitterShowUserOptions({access_token: result.data.access_token, screen_name: screen_name});
        mx.testTwitterUserShow(topt5)
      }
    }
    mx.testTwitterAuth(topt4)
  } else {
    console.log('no doubleCheck option skipping testTwitterUserShow and testTwitterFollowersIds with testTwitterAuth')
  }

}
