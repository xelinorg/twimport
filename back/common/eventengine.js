const mx = module.exports = {};

function EventEngine(option){
  const config = {};
  const subscribers = {};
  const watching = {};
  this.configf = option.configf(config);
  this.subscribe = option.configf(subscribers);
  this.watch = option.configf(watching);
  this.configUpdate = function(option){
    // if operator id in option and match with the entry on the config and socket are the same is the owner
    // so let update happen
    console.log('a subscriber requested configUpdate')
    if(typeof option === 'object' && typeof option.req === 'object' && typeof option.res === 'object'){

      const key = option.req.query.key;
      const secret = option.req.query.secret;
      const watch = option.req.query.watch;
      const watchResponse = watching[watch];

      if(typeof subscribers[key] === 'object' && subscribers[secret] === key && typeof watchResponse === 'object' && watchResponse.length > 0){
        let foundSubscriber = -1;
        watching[watch].forEach(function(watcher, index){
          if(Object.is(watcher.socket.req, option.req)){
            foundSubscriber=index
          }
        })
        watching[watch].splice(foundSubscriber, 1)
        delete subscribers[secret];
        delete subscribers[key];
      }else{
        console.log('configUpdate request does not match existing')
      }

    }else{
      console.log('EventEngine configUpdate option needs operator and socket to update')
    }
  };
  this.esheaders = {
    'Content-Type': 'text/event-stream',
    'Cache-Control': 'no-cache',
    'Connection': 'keep-alive'
  };
  this.pingon = 0;
  this.pingInterval = option.pingInterval || 30000;
  this.startmark = 'data:';
  this.endmark = '\n\n'
};

EventEngine.prototype.jsonwire = function jsonwireEventEngine(payload){
  const sm = this.startmark;
  const em = this.endmark;

  if (typeof payload === 'object' && payload.length === undefined){
    return sm + JSON.stringify(payload) + em
  } else if(typeof payload === 'object' && payload.length > 0){
    return payload.reduce(function(bucket, op){
      bucket += sm + JSON.stringify(op) + em
      return bucket
    },'')
  }

};

EventEngine.prototype.pinger = function pingerEventEngine(){
  if (this.pingon>0){
    return
  }
  const subscribe = this.subscribe.bind(this);
  const jsonwire = this.jsonwire.bind(this);
  const pinterval = this.pingInterval;

  function pingSubscribers(){
    setInterval(function(){
      Object.entries(subscribe()).forEach(function(item){
        if (typeof item[1] === 'object' ){
          const ping ={
            op: 'ping',
            ts: Date.now()
          };
          const out = jsonwire(ping)
          item[1].write(out)
        }
      });
    }, pinterval)
  }

  pingSubscribers()
  this.pingon= 1;
}

EventEngine.prototype.register = function registerEventEngine(option){

  const oreq = option.req;
  const ores = option.res;
  const configUpdate = this.configUpdate.bind(this);

  if (oreq.method === 'GET' && oreq.headers['accept'].indexOf('text/event-stream') > -1  && typeof oreq.query === 'object' && typeof oreq.query.key === 'string'){

    const haspath = this.configf('topology').filter(function(path){
      return path === oreq.path
    });

    if(haspath.length === 0){
      return ores.status(400).send('no events on this path')
    }

    if(this.subscribe(oreq.query.key) && this.subscribe(oreq.query.secret) !== oreq.query.key){
      return ores.status(403).send('sse key exists ' + oreq.query.key);
    }else{
      //configUpdate({req: oreq, res: ores})
      ores.set(this.esheaders);
      this.subscribe({key: oreq.query.key, value: ores});
      this.subscribe({key: oreq.query.secret, value: oreq.query.key});
      if (oreq.query.watch){
        if (this.watch(oreq.query.watch)){
          // watch exists, push the new subscriber
          this.watch(oreq.query.watch).push(ores);
        } else {
          this.watch({key: oreq.query.watch, value:[ores]});
        }

      }
      ores.on('end',function(){
        console.log('sse middleware res on end', oreq.query)
      });
      ores.on('close',function(){
        // client closed the connection we have to remove from the engine the socket but keep the key and secret
        console.log('sse middleware res on close', oreq.query)
        configUpdate({req: oreq, res: ores})
      });
    }

  }

};

EventEngine.prototype.next = function nextEventEngine(option){
  // here we have to register the sse path and query for the engine to know later
  // how to handle the events when these are asked by the succes of the api various calls

  const payload = this.jsonwire(option.payload);
  const container = {
    headers: this.esheaders,
    payload: payload
  };
  return new Promise(function(resolve, reject){
    if(typeof option === 'object' &&  option.action.method === 'GET' && typeof option.payload === 'object'){
      resolve(container)
    }else{
      return reject('sse next error')
    }
  })

};

EventEngine.prototype.eventreader = function eventreaderEventEngine(option){
  console.log('EventEngine.prototype.eventreader', option)
  return new Promise(function(resolve, reject){
    if(typeof option === 'object' &&  typeof option.key === 'string' && typeof option.secret === 'string'){
      const status = {op: 'register', status: 1};
      resolve([status])
    }else{
      return reject('sse eventreader error')
    }
  })
}

EventEngine.prototype.endHook = function endHookEventEngine(option){
  console.log('EventEngine.prototype.endHook', option)

  const jsonwire = this.jsonwire.bind(this);
  const watch = this.watch.bind(this);
  const repeater = this.repeater.bind(this);

  return new Promise(function(resolve, reject){
    if(typeof option === 'object' &&  typeof option.action === 'object'){
      const hookdata = jsonwire({op:'hook', withOption: option});
      const subscribers = watch(option.action.endpoint + '/' + option.action.method);
      if (typeof subscribers === 'object' && subscribers.length > 0) {
        return resolve({payload:hookdata, subscribers: subscribers, repeater: repeater})
      }
      return reject('subscriber has closed')
    }else{
      return reject('sse endHook error')
    }
  })
}

EventEngine.prototype.repeater = function repeaterEventEngine(option){
  option.subscribers.forEach(function(subscriber){
    if (option.provide(option.entity, subscriber.req.url)){
      subscriber.write(option.payload)
    }
  })
}

EventEngine.prototype.topology = function topologyEventEngine(option){
  const topology = Object.entries(option).reduce(function(bucket, item){
    if (item[1].resolve === 'sse'){
      bucket.push('/' + item[0])
    };
    return bucket
  }, []);

  this.configf({key:'topology', value: topology})
};

mx.createEventEngine = function(option){
  if (typeof option !== 'object' || typeof option.configf !== 'function'){
    return
  }
  const opt = {};
  opt.configf = option.configf;
  const ee = new EventEngine(opt);
  return ee
}
