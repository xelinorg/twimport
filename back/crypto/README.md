#### generate required crypto files with the following openssl command
(.pem ending files are ignored in .gitignore, we need to do something with other cryptofiles)

```
cd crypto

openssl req -newkey rsa:2048 -new -nodes -keyout server.key -out certificate-sign-request.pem

openssl x509 -req -days 365 -in certificate-sign-request.pem -signkey server.key -out server.cert

cp server.cert server.pem
```
