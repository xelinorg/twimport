const mx = module.exports = {};

function register(option){
  const action = option['action'](option);
  const opt = option[action.endpoint];
  opt.router.get(opt.level + action.endpoint, function (req, res, next) {
    res.send({status: 'up and running'});
    return next()
  });
}

mx.register = register
