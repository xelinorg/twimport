const mx = module.exports = {};

mx.connect = connect;

const endpoints = {

  'status': { },
  'import': {
    resolve : 'db',
    hook: {
      'POST': {
        provide: function(entity, urlname){
          const matchKey = '&twid='
          if (urlname.indexOf(matchKey + '*') > -1) {
            return true
          }
          const matchToken = matchKey + entity.twid
          return urlname.indexOf(matchToken) > -1
        }
      }
    }
  } ,
  'loop': {
    resolve: 'sse',
    reader: 'sse'
  }
};

function packOption(option){

  const successOpt = {
    handler: option.resolveHandlers[option.endpoint.resolve]
  };

  if ( typeof option.endpoint.hook === 'object' && Object.keys(option.endpoint.hook).length > 0 ){
    successOpt['endHook'] = option.endpoint.hook
  }

  const resolver = success(successOpt);
  const reader = option.readers[option.endpoint.reader || 'default'];

  return  {
    level: option.level || '/',
    router: option.router,
    reader: reader,
    resolve: resolver,
    reject: option.endpoint.reject || failure
  }

};

function setAction(option){
  const endpoint = Object.entries(option).reduce(function(bucket, endpoint){
    if(endpoint[0] !== 'action'){
      bucket = endpoint[0];
    }
    return bucket
  },'')

  return {
    endpoint: endpoint,
    method: null
  }

};

function success(option){
  const apiNext = option.handler ? option.handler.next.bind(option.handler) : function api500(res){res.status(500).send('')} //we have no handler this is our fault
  return function successInner(action, res, endpointResult){
    return apiNext({action:action, payload:endpointResult}).then(
      function apiNextSuccess(result){
        //console.log('inner result is\n', result, '\n')
        if (!result.headers){
          res.send({action:action, endpointResult:endpointResult, result: result})
        }else{
          // this is raw write and we set status and write the body, express can only end now
          res.writeHead(200, result.headers);
          res.write(result.payload);
        }

        if(option.endHook && option.endHook[action.method] && typeof option.endHook[action.method].provide === 'function'){
          const hook = option.endHook[action.method];
          hook.fire({action:action, endpointResult:endpointResult}).then(
            function(hookResult){
              //console.log('api index hookResult', hookResult)
              hookResult.repeater({payload: hookResult.payload, subscribers: hookResult.subscribers, provide: hook.provide, entity: endpointResult})
            },
            function(hookError){
              // we dont handle sse errors here yet
              console.log('api index hookError', hookError)
            }
          ) // this is ugly as well
        }

      },
      function apiNextFailure(error){
        console.log('successInner error on next', error)
      }
    )
  }
}

function failure(action, res, err){
  //console.log('failure', action, res, err)
  return res.status(400).send(err);
  //res.status(400).end()
}

function connect(option){

  option.ssehandler.topology.bind(option.ssehandler)(endpoints);

  const resolveHandlers = {
    sse: option.ssehandler,
    db: option.dbhandler
  }

  const readers = {
    sse : option.ssehandler.eventreader.bind(option.ssehandler),
    default: option.reader
  }

  Object.entries(endpoints).forEach(function(ep){
    const epKey = ep[0];
    const epValue = ep[1];
    const opt = packOption({router: option.router, endpoint: epValue, resolveHandlers: resolveHandlers, readers: readers})
    const container = {
      action: setAction,
    };
    container[epKey] = opt;

    epValue.hook && (epValue.hook.POST.fire = option.ssehandler.endHook.bind(option.ssehandler));

    const m = require('./' + epKey);
    m.register(container)
  })
}
