const mx = module.exports = {};

function register(option){

  const action = option['action'](option);
  const opt = option[action.endpoint];

  opt.router.post(opt.level + action.endpoint, function (req, res, next) {

    action.method = req.method;

    opt.reader(req).then(
      function importSuccessRead(result){
        if (!result.twid || typeof result.twid !== 'string'){
          return opt.reject(action, res, 'no twid provided');
        }
        return opt.resolve(action, res, result);

      },
      function importFailedRead(err){
        return opt.reject(action, res, err);
      }
    )

  });

  opt.router.get(opt.level + action.endpoint, function (req, res, next) {
      action.method = req.method;
      const nextpayload = req.query.twid ? {twid :req.query.twid} : {};
      return opt.resolve(action, res, nextpayload)
  })

}

mx.register = register
