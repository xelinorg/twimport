const mx = module.exports = {};

function register(option){

  const action = option['action'](option);
  const opt = option[action.endpoint];

  opt.router.post(opt.level + action.endpoint, function (req, res, next) {

    action.method = req.method;
    return opt.reject(action, res, 'no post on loop');

  });

  opt.router.get(opt.level + action.endpoint, function (req, res, next) {
    //console.log('sse number of clients  :', Object.entries(clients).length)
      action.method = req.method;
      if (!req.query){
        return opt.reject(action, res, 'empty query parameters');
      }

      opt.reader(req.query).then(
        function importSuccessRead(result){
          return opt.resolve(action, res, result);

        },
        function importFailedRead(err){
          return opt.reject(action, res, err);
        }
      )

  })

}

mx.register = register
