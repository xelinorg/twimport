const storage = require('./storage');
const api = require('./api');
const common = require('./common');

const mem = storage.memory.malloc();

function haltExpress(option){
  const delay = option && option.delay > 0 ?  option.delay : 3000;
  setTimeout(function () {
    mem.last('server').close();
    console.log('terminated')
  }, delay)
};

function bootExpress(option){

  if (typeof option === 'object' && typeof option.createServer === 'function'){

    const express = require('express');
    const router = express.Router();
    const dbhandler = storage.mongodbref.createDB();
    const sseengine = common.createEventEngine({configf: common.configf});

    const ssepinger = sseengine.pinger.bind(sseengine);
    ssepinger()

    const app = express();
    app.use(common.net.middleware.cors);
    app.use(common.net.middleware.sse({engine: sseengine, router:router}));
    app.use(router);

    api.connect({router:router, reader:common.net.reader, dbhandler: dbhandler, ssehandler: sseengine});

    const port = option && option.port > 1024 ? option.port : 3000;
    const server = option.createServer(app);
    server.listen(port, function () {
      console.log('ready and listening on port ' + port)
    });
    mem.alloc('server', server)

  } else {
    console.log('can not boot express without http createServer function')
  }

};

module.exports = {
  common: common,
  storage: storage,
  api: api,
  bootExpress: bootExpress,
  haltExpress: haltExpress
}

//
// boot as a node process
//

if (require.main === module) {

  const setBootOps = function setBootOps(option, requiredBootOpts, realBootops){

    return requiredBootOpts.filter(function(opt){
      const foundOpt = realBootops[opt];
      if (foundOpt){
        option[opt] = foundOpt;
        return true
      }
      return false
    }).length === requiredBootOpts.length

  }

  const requiredBootOpts = [
    'WITH_EXPRESS',
    'USE_CRYPTO'
  ];

  const realBootops = process.argv.reduce(function(ops, op){
    const keyVal = op.split("=");
    if(keyVal){
      ops[keyVal[0]] = keyVal[1];
    }
    return ops
  }, {})

  const option = {}

  if (setBootOps(option, requiredBootOpts, realBootops)){
    if (option['USE_CRYPTO']){
      const fs = require('fs');
      const https = require('https');
      const privateKey  = fs.readFileSync('crypto/server.key');
      const certificate = fs.readFileSync('crypto/server.pem');
      const credentials = {key: privateKey, cert: certificate};

      option.createServer = function(app){
        return https.createServer(credentials, app)
      }
    } else {
      var http = require('http');
      option.createServer = http.createServer;
    }
    if(option['WITH_EXPRESS']){
      bootExpress(option)
    }
  } else {
    console.log("required options not provided")
  }

}
