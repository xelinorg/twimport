import React from 'react';

function ApiResource(option){
  const opt = option || {};
  this.base = opt.base || 'https://localhost:3000';
  this.path = opt.path || '/'
}

ApiResource.prototype.get = function(option){
  return new Promise((resolve, reject) => {
    if (option) {
      return resolve({ApiResourcePrototypeGet: option})
    } else {
      return reject('ApiResource.prototype.save reject, you passed no options!?')
    }
  })
}

ApiResource.prototype.save = function(option){
  const saveOption = {
    method: 'POST',
    path: this.base + this.path,
    payload: option.payload
  }

  return new Promise((resolve, reject) => {
    saveOption['resolve'] = resolve;
    saveOption['reject'] = reject;
    makeRequest(saveOption)
  })
}

function makeRequest(option) {
  const httpRequest = new XMLHttpRequest();

  if (!httpRequest) {
    return option.reject('No XMLHttpRequest on ApiResource module');
  }
  httpRequest.onreadystatechange = function() {
    if (httpRequest.readyState === 4) {
      return option.resolve(httpRequest.response)
    }
  }

  httpRequest.onerror = function(error){
    return option.reject('XMLHttpRequest onerror ApiResource module', error);
  };
  httpRequest.onabort = function(error){
    return option.reject('XMLHttpRequest onabort error ApiResource module', error);
  };
  httpRequest.onprogress = function () {
    console.log('XMLHttpRequest onprogress', httpRequest.status);
  };
  httpRequest.ontimeout = function(error) {
    return option.reject('XMLHttpRequest ontimeout error ApiResource module', error);
  };

  httpRequest.open(option.method, option.path, true);
  httpRequest.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
  httpRequest.send(JSON.stringify(option.payload))
}

function getResource(props){
  return {
    endpoint: new ApiResource({path: props.match.path})
  }
}

export default function withApiResource(WrappedComponent) {
  class ApiResource extends React.Component {
    render() {
      return <WrappedComponent {...this.props } resource={getResource(this.props)}/>
    }
  }

  return ApiResource;
}
