import withApiResource from './with-api-resource'
import withLog from './with-log'

export {
  withApiResource,
  withLog
}
