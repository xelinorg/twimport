import React from 'react';

const StatusContext = React.createContext({
  status: 0,
  userID: 0,
  toggleStatus: () => {},
  tripleA: () => {}
});

export {
  StatusContext
}
