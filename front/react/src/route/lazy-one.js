import React, { Component } from 'react';

class LazyOne extends React.Component {

    componentWillMount() {
      if (!this.state) {
        this.setState({ lazyOneLoaded: 'loading...' })
      }
    }

    async componentDidMount() {
      const onMountSetState = this.setState.bind(this)
      const timeout = setTimeout(()=>{
        onMountSetState({ lazyOneLoaded: 'loaded!!!' });
      }, 900)
      this.setState({timeout:timeout})
    }

    componentWillUnmount(){
      clearTimeout(this.state.timeout);
    }

    render() {
      const lazyOneLoaded = this.state.lazyOneLoaded
      if (lazyOneLoaded) {
          return <section className="default-space"><p>{lazyOneLoaded}</p></section>
      }
      return null
    }
}

export default LazyOne
