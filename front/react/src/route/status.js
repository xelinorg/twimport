import React from 'react';

import { StatusContext } from '../context'

class Status extends React.Component {
  constructor(props) {
    super(props);
    this.handleMouseMove = this.handleMouseMove.bind(this);
    this.state = { x: 0, y: 0 };
  }

  handleMouseMove(event) {
    this.setState({
      x: event.clientX,
      y: event.clientY
    });
  }

  render() {
    return (
      <section className="default-space">
        <StatusContext.Consumer>
          {({toggleStatus}) => (
            <div style={{ height: '100%' }} onMouseMove={this.handleMouseMove}>
              <p>Move the mouse around!</p>
              <p>The current mouse position is ({this.state.x}, {this.state.y})</p>
              <hr/>
              <button
                onClick={toggleStatus}>
                Toggle Status
              </button>
            </div>
          )}
        </StatusContext.Consumer>
      </section>
    );
  }
}

Status.contextType = StatusContext;

export default Status
