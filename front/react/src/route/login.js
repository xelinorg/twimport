import React, { Component} from 'react';
import { FacebookProvider, Subscribe, LoginButton } from 'react-facebook';

import { StatusContext } from '../context'

class Login extends React.Component {

  constructor(props) {
    super(props);
    this.state = {};
    this.state.connectedOnce = 0;
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(context){
    const setState = this.setState.bind(this)
    return function bindTripleA(data){
      if (data.status === 'connected'){
        console.log('Login handleChange FacebookProvider connected...')
        setState({connectedOnce:1})
        context.tripleA(data.authResponse)
      }else{
        console.log('trying to handle Login auth.statusChange event when !== to connected...')
        setState(state=>(
          console.log(state)
        ))
      }
    }
  }

  render() {
    return (
      <section className="default-space pad-top-3">
        <p>connect your social network account</p>
        <StatusContext.Consumer>
        {(tripleA)=>(
          <FacebookProvider appId="1733165480028177">
            <Subscribe event="auth.statusChange" onChange={this.handleChange(tripleA)}>
              <LoginButton scope="email">
                <span>Login via Facebook</span>
              </LoginButton>
            </Subscribe>
          </FacebookProvider>
        )}
        </StatusContext.Consumer>
      </section>
    );
  }
}

Login.contextType = StatusContext;

export default Login
