import React from 'react';

class ImportInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: ''
    }
    this.onValueChange = this.inputValueChange.bind(this)
  }

  inputValueChange(event){
    if (this.state.value !== event.target.value) {
      this.setState({value: event.target.value})
      this.props.onValueChange(event)
    }
  }

  render() {
    return <input type="text" value={this.state.value} onChange={this.onValueChange} />
  }
}

class ImportSubmit extends React.Component {
  render() {
    return <input type="submit"/>
  }
}

class ImportForm extends React.Component {

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.save = props.resource.save.bind(props.resource);
    this.state = {}
  }

  handleChange(event) {
    this.setState({importHandle: event.target.value})
  }

  handleSubmit(event) {
    event.preventDefault()
    if (typeof this.state.importHandle === 'string'){
      const payload = {twid:this.state.importHandle}
      this.save({payload}).then((data) => {
        this.setState({submitResponse: data})
      })
    }
  }

  render() {
    return (
      <section className="default-space pad-top-3">
        <p>import a twitter handle for followers crawling</p>
        <form onSubmit={this.handleSubmit}>
          <ImportInput onValueChange={this.handleChange}/>
          <ImportSubmit/>
        </form>
        <p>{this.state.submitResponse}</p>
      </section>
    );
  }
}

function Import(props) {
  return (
    <ImportForm resource={props.resource.endpoint}/>
  );
}

export default Import
