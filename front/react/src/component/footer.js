import React from 'react';

import { StatusContext } from '../context'

class Footer extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <StatusContext.Consumer>
        {({userID}) => (
          <div>
            <h6>AAA : {userID}</h6>
          </div>
        )}
      </StatusContext.Consumer>
    );
  }
}

Footer.contextType = StatusContext;

export default Footer
