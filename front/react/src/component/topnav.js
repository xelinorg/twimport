import React from 'react';
import { Link, NavLink } from 'react-router-dom';

import { StatusContext } from '../context'

class TopNav extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <StatusContext.Consumer>
        {({userID, status}) => (

          <nav className={(status ? '' : 'not-active')}>
            <ul>
              <li>
                <Link to="/">Login</Link>
              </li>
              <li>
                <NavLink to="/import/" activeClassName="selectedNavLink">Import</NavLink>
              </li>
              <li>
                <NavLink to="/lazy/" activeClassName="selectedNavLink">LazyOne</NavLink>
              </li>
              <li>
                <NavLink to="/vanilla/" activeClassName="selectedNavLink">Vanilla</NavLink>
              </li>
              <li>
                <NavLink to="/status/" activeClassName="selectedNavLink">
                  Status
                </NavLink>
              </li>
              <li>
                <p>Navigation is {(status ? 'On' : 'Off')}</p>
              </li>
            </ul>
          </nav>

        )}
      </StatusContext.Consumer>
    );
  }
}

TopNav.contextType = StatusContext;

export default TopNav
