import React, { Suspense, lazy } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import './style.css';

import { StatusContext } from './context'
import { withApiResource, withLog } from './common'
import { Footer, TopNav } from './component'
import { Login, Import, Status, Vanilla } from './route';

// webpackChunkName does not seam to work, exists here as a reminder to test it more
const LazyOne = React.lazy(() => import(/*webpackChunkName: 'LazyOne' */ './route/lazy-one'));

const LogSwitch = withLog(Switch);

class App extends React.Component {

  constructor(props){
    super(props);

    this.toggleStatus = () =>{
      this.setState(state=>({
        status: state.status ? 0 : 1
      }))
    }
    this.tripleA = (authResponse) =>{
      if (authResponse && authResponse.userID){
        this.setState(state=>({
          userID: authResponse.userID
        }))
      }
    }
    this.state = {
      status: 1,
      userID: 0,
      toggleStatus: this.toggleStatus,
      tripleA: this.tripleA
    }
  }

  render() {
    return (
      <StatusContext.Provider value={this.state}>
        <Router>
          <div>
            <TopNav/>
            <Suspense fallback={<section className="default-space"><p>...loading</p></section>}>
              <LogSwitch>
                <Route exact path="/" component={Login}/>
                <Route path="/import" component={withApiResource(Import)}/>
                <Route path="/status" component={Status}/>
                <Route path="/lazy" render={props => <LazyOne {...props} />}/>
                <Route path="/vanilla" component={Vanilla}/>
              </LogSwitch>
            </Suspense>
          </div>
        </Router>
        <Footer/>
      </StatusContext.Provider>
    )
  }
}


ReactDOM.render(<App/>, document.getElementById('rootEl'));
