// expose local storage and http request aka net
require('angular-resource');
require('./api');
require('./event');

var common = angular.module('common', ['api', 'event']);
