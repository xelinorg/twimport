require('angular-resource');
var apim = angular.module('api', ['ngResource']);

apim.service('Imports', ['$resource', function($resource) {
  return $resource('http://localhost:3000/import/');
}]);
