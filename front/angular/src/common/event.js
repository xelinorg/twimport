var eventm = angular.module('event', []);

eventm.service('Events', ['$rootScope', function($rootScope){

  const srv = this;
  const reg = {};

  srv.register = function(token){
    reg[token.watch] = new EventSource('http://localhost:3000/loop?key=ang&secret=gna&watch=' + token.watch, {withCredentials:true});
    reg[token.watch].onmessage = function(e) {
      $rootScope.$broadcast(token.watch, e.data)
    }
    reg[token.watch].onerror = function(err) {
      reg[token.watch].close()
    }

  }

  return {
    register: srv.register
  }

}])
