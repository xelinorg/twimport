const progressm = angular.module('progress', ['ngRoute', 'api']);

progressm.config(['$routeProvider', '$locationProvider',
    function config($routeProvider, $locationProvider) {

      $routeProvider.
        when('/progress', {
          templateUrl: '../progress/progress.html',
          controller: 'ProgressController',
          controllerAs: 'vm'
        });
    }
  ]
);

progressm.controller('ProgressController', ['$rootScope', 'Imports', 'Events',  function($rootScope, Imports, Events){
  //console.log('ImportController hit!')
  const vm = this;

  const imps = Imports.get();
  imps.$promise.then(function(res){
    vm.twitterHandles = res.result
  })

}]);
