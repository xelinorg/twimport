const angular = require('angular');
require('angular-route');
require('angular-material')
require('angular-messages')
require('./login');
require('./common')
require('./import');
require('./progress');
require('./wizard');
require('angular-material/angular-material.css')
require('material-design-icons/iconfont/material-icons.css')

const appm = angular.module('twimport', ['wizard', 'login', 'import', 'progress', 'ngRoute', 'event', 'ngMaterial', 'ngMessages']);

const defaultRoute = '/import';
const rootRoute = '/';

appm.controller('MainController', ['$rootScope', '$location', '$scope', 'Events',  function($rootScope, $location, $scope, Events){
  // console.log('MainController hit!')

  $rootScope.$on('fblogout', function(){
    if($location.path() !== rootRoute){
      $rootScope.$apply(function(){
        $location.path(rootRoute)
      })
    }
  })

  $rootScope.$on('fblogin', function(){
    if($location.path() === rootRoute){
      $location.path(defaultRoute)
    }
    if($location.path() !== rootRoute){
      $rootScope.$apply(function(){
        $location.path($location.path())
      })
      $location.path(defaultRoute)
    }
  })

  $rootScope.$on('import/POST', function(ev, data){
    console.log('$rootScope.$on:import/POST', JSON.parse(data))
  })

  Events.register({watch:'import/POST'})

}]);

appm.run(['$rootScope', '$window', '$location', 'fbauth',function($rootScope, $window, $location, fbauth) {
  //console.log('app run hit!')
  let toRememberRoute;

  const fbopt = {
    appId: '1733165480028177',
    version    : 'v3.2',
    channelUrl: '/login/login.html',
    status: true,
    cookie: true,
    xfbml: true
  };

  $window.fbAsyncInit = function() {
    FB.init(fbopt);
    fbauth.getStatus().then(
      function(res){
        // there is an authentication response check if user is connected
        if (!res.authResponse || res.status !== 'connected'){
          $location.path(rootRoute)
        }else{
          // if user is connected and is routed to root path, route to the default success path aka import for now
          if($location.path() === rootRoute){
            $location.path(defaultRoute)
          }else{
            if(toRememberRoute){
              const fixedUrl = toRememberRoute.originalPath.match(toRememberRoute.regexp)
              fixedUrl.slice(1).forEach(param => {
                //console.log(param)
                fixedUrl[0] = fixedUrl[0].replace(param, toRememberRoute.params[param.replace(':', '')])
              })
              $location.path(fixedUrl[0]);
              toRememberRoute = null
            }
          }
        }
      },
      function(err){
        // fbauth rejected send user to root path
        $location.path(rootRoute)
      }
    )

  }

  $rootScope.$on('$routeChangeStart', function(ev, next, current){
    if (toRememberRoute && current && current.originalPath === rootRoute){
      console.log('there is route remembered', toRememberRoute);
    }

    const routepair={};
    if (next && next.originalPath){
      routepair.next = next
    }
    if (current && current.originalPath){
      routepair.current = current
    }
    if(routepair.next && !routepair.current && routepair.next.originalPath !== rootRoute){
      //user is coming from outterspace!
      toRememberRoute = routepair.next;
      // user has reloaded from outside the app and we have to route through
      // the root path to load auth service
      $location.path(rootRoute)
    }
    //console.log('routeChangeStart fired!', routepair)

  })

  $rootScope.$on('$routeChangeSuccess', function(ev, next, current){
    const routepair={};
    if (next && next.originalPath){
      routepair.next = next
    }
    if (current && current.originalPath){
      routepair.current = current
    }
    //console.log('routeChangeSuccess fired!', routepair)
    if(toRememberRoute){

    }
  })

  $rootScope.$on('$routeChangeError', function(ev, next, current){
    const routepair={};
    if (next && next.originalPath){
      routepair.next = next
    }
    if (current && current.originalPath){
      routepair.current = current
    }
    //console.log('$routeChangeError fired!', routepair, toRememberRoute)
  })

  (function(d){
    // load the Facebook javascript SDK
    var js,
    id = 'facebook-jssdk',
    ref = d.getElementsByTagName('script')[0];

    if (d.getElementById(id)) {
      return;
    }

    js = d.createElement('script');
    js.id = id;
    js.async = true;
    js.src = "https://connect.facebook.net/en_US/sdk.js";

    ref.parentNode.insertBefore(js, ref);
  }(document));

}]);

appm.config(['$routeProvider', '$locationProvider',
    function config($routeProvider, $locationProvider) {

      $routeProvider.
        when('/', {
          template: '<h4>twimport0.0.1</h4>'
        }).
        otherwise('/');
        $locationProvider.html5Mode(true);
    }
  ]);
