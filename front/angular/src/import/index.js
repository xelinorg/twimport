
const importm = angular.module('import', ['ngRoute', 'login', 'api']);

importm.config(['$routeProvider', '$locationProvider',
    function config($routeProvider, $locationProvider) {

      $routeProvider.
        when('/import', {
          templateUrl: '/import/import.html',
          controller: 'ImportController',
          controllerAs: 'vm',
          resolve:{
            uid: ['fbauth', function(fbauth){
              //console.log('import resolve uid hit')
              return fbauth.fbuid(
                function(res){
                  return res.authResponse.userID
                },function(err){
                  return err
                }
              )
            }]
          }
        });
    }
  ]
);

importm.controller('ImportController', ['$rootScope', '$scope', 'Imports',  function($rootScope, $scope, Imports){
  //console.log('ImportController hit!')
  const vm = this;
  vm.twitterHandle = {
    screenName: null,
    uid: null
  };

  const imp = Imports.get();

  vm.submitHandle = function(handle){
    if(handle && typeof handle.screenName === 'string' && typeof handle.uid === 'string'){
      //do http call to request import of the twitter handle
      Imports.save({twid:handle.screenName});
    }
  }

}]);
