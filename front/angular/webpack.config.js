const path = require('path')

const HtmlWebPackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const htmlPlugin = new HtmlWebPackPlugin({
  template: 'src/index.html',
  filename: 'index.html'
});

const copyHtml = new CopyWebpackPlugin(
  [{
    from: 'src/**/*.html',
    to: './', flatten: false,
    transformPath (targetPath, absolutePath) {
      return targetPath.replace('src', '');
    }
  }])

module.exports = {
  entry: [
    './src/index.js',
  ],
  output: {
    path: path.resolve('dist'),
    publicPath: '/',
    filename: 'bundle.js',
    chunkFilename: '[name].bundle.js'
  },
  devtool: 'inline-source-map',
  devServer:{
    https: true,
    disableHostCheck: true,
    historyApiFallback: true,
    contentBase: './src',
    watchContentBase: true,
  },
  plugins: [
    htmlPlugin,
    copyHtml
  ]
};
